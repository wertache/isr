from nltk import word_tokenize, pos_tag
from gutenberg.acquire import load_etext
from gutenberg.cleanup import strip_headers
from os import path

book_title = str(input('title of the book '))
book_author = str(input('author of the book '))
book_author_sex = str(input('gender of the author '))
book_code = int(input('gutenberg\'s index of the book '))

try:
    book_text = strip_headers(load_etext(book_code))
except:
    print('Please enter a valid index')
else:
    book_text_tok = word_tokenize(book_text, 'english')
    sample = book_text_tok[:10000]
    sample_pos = pos_tag(sample)
    sample_size = len(sample)
    
    total_pron = 0
    fp_pron = 0
    tp_pron = 0
    for word in sample_pos:
        if word[1] == 'PRP' or word [1] == 'PRP$':
            total_pron += 1
            if word[0].lower() in ['i', 'me','my','mine','myself','we','us','our','ours','ourselves']:
                fp_pron += 1
            if word[0].lower() in ['he','him','his','himself','she','her','hers','herself','they','them','their','theirs','themselves']:
                tp_pron += 1

    print("In this sample there are:\nA total of {0} pronouns \n{1} first person pronouns \n{2} third person pronouns".format(total_pron,fp_pron,tp_pron))

    if not path.isfile('./data.csv'):
        file = open('data.csv','a')
        file.write('title, author, gender, total pronouns, first person pronouns, third person pronouns, sample size\n')
        file.close()

    file = open('data.csv','a')

    file.write('{0},{1},{2},{3},{4},{5},{6} words\n'.format(book_title,book_author,book_author_sex, total_pron,fp_pron,tp_pron,sample_size))

    file.close 
