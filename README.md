To use the python script, you need to install the Pypi Gutenberg Library and NLTK.
The script will prompt you to insert metadata on the book you want to add to your dataset. You can find the code Gutenberg requires on Gutenberg.org
You can change sample size in the set itself.


To use the analysis sheet, you need spreadsheet software like Microsoft Excel. A free alternative is Google Spreadsheets. All you need to do is add data.csv, generated by the python script, to the collective sheet. All references have been set and graphs will automatically be generated. If you want reflective data on the actual narrative perspective, you will have to add those manually on the narrative perspective analysis sheet. necessary instructions are given on the sheet itself.
